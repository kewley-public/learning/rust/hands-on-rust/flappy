use rodio::OutputStream;
use rodio::{source::Source, Decoder, OutputStreamHandle};
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;

pub struct AudioManager {
    stream_handle: OutputStreamHandle,
    sounds: HashMap<String, PathBuf>,
}

impl AudioManager {
    pub fn new(stream_handle: OutputStreamHandle) -> Self {
        Self {
            stream_handle,
            sounds: HashMap::new(),
        }
    }

    pub fn add_sound(mut self, name: &str, file_path: PathBuf) -> Self {
        self.sounds.insert(name.to_string(), file_path);
        self
    }

    pub fn play_sound(&self, name: &str, volume: f32) {
        if let Some(file_path) = self.sounds.get(name) {
            match File::open(file_path) {
                Ok(file) => {
                    let file = BufReader::new(file);
                    match Decoder::new(file) {
                        Ok(source) => {
                            let amplified = source.amplify(volume);
                            self.stream_handle
                                .play_raw(amplified.convert_samples())
                                .expect("Failed to play audio");
                        }
                        Err(e) => {
                            eprintln!("Failed to decode audio: {}", e);
                        }
                    }
                }
                Err(e) => {
                    eprintln!("Failed to open audio file: {}", e);
                }
            }
        } else {
            eprintln!("Sound not found: {}", name);
        }
    }
}

pub fn get_default_audio_device() -> Option<(OutputStream, OutputStreamHandle)> {
    match OutputStream::try_default() {
        Ok((stream, stream_handle)) => Some((stream, stream_handle)),
        Err(e) => {
            eprintln!("Failed to get default audio device: {}", e);
            None
        }
    }
}
