#![warn(clippy::all, clippy::pedantic)]

use bracket_lib::prelude::*;
use flappy::get_default_audio_device;

use flappy::State;
use flappy::SCREEN_HEIGHT;
use flappy::SCREEN_WIDTH;

// Making main return a BError allows us to take advantage of ? operator
fn main() -> BError {
    let flappy_asset = "flappy32.png";

    let context = BTermBuilder::new()
        .with_title("Flappy Dragon")
        .with_font(flappy_asset, 32, 32)
        .with_simple_console(SCREEN_WIDTH, SCREEN_HEIGHT, flappy_asset)
        .with_fancy_console(SCREEN_WIDTH, SCREEN_HEIGHT, flappy_asset)
        .with_tile_dimensions(16, 16)
        .build()?;

    if let Some((_, stream_handler)) = get_default_audio_device() {
        main_loop(context, State::new(stream_handler))
    } else {
        eprintln!("No audio device available");
        std::process::exit(1);
    }
}
