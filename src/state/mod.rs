use std::path::PathBuf;

use crate::components::prelude::*;
use crate::types::prelude::*;
use bracket_lib::prelude::*;
use rodio::OutputStreamHandle;

add_wasm_support!();

#[derive(Clone, Debug)]
enum GameMode {
    Menu,
    Playing,
    End,
}

pub struct State {
    player: Player,
    frame_time: f32,
    obstacle: Obstacle,
    score: i32,
    mode: GameMode,
    audo_volume: f32,
    audio_manager: AudioManager,
    ground: Ground,
}

impl State {
    pub fn new(stream_handle: OutputStreamHandle) -> Self {
        let audio_manager = AudioManager::new(stream_handle)
            .add_sound(FLAP_SOUND, PathBuf::from("assets/audio/flap.wav"))
            .add_sound(GAINED_POINT_SOUND, PathBuf::from("assets/audio/point.wav"))
            .add_sound(GAME_END_SOUND, PathBuf::from("assets/audio/ending.wav"));
        Self {
            player: Player::new(5, 25),
            frame_time: 0.0,
            obstacle: Obstacle::new(SCREEN_WIDTH, 0),
            score: 0,
            mode: GameMode::Menu,
            audo_volume: 32.0,
            audio_manager,
            ground: Ground::new(SCREEN_HEIGHT - 1),
        }
    }

    pub fn play(&mut self, ctx: &mut BTerm) {
        ctx.cls_bg(NAVY);
        // Either 30 or 60 fps
        self.frame_time += ctx.frame_time_ms;

        // Wait for 75fps or so to pass then process, allows for players to react accordingly
        if self.frame_time > FRAME_DURATION {
            self.frame_time = 0.0;

            self.player.gravity_and_move();
        }

        if let Some(VirtualKeyCode::Space) = ctx.key {
            self.player.flap();
            self.audio_manager.play_sound(FLAP_SOUND, self.audo_volume);
        }

        ctx.set_active_console(1);
        ctx.cls();
        self.player.render(ctx);
        self.obstacle.render(ctx, self.player.x);
        self.ground.render(ctx);
        ctx.set_active_console(0);

        ctx.print(0, 0, "Press SPACE to flap.");
        ctx.print(0, 1, &format!("Score: {}", self.score));

        if self.player.x > self.obstacle.x {
            self.score += 1;
            self.audio_manager
                .play_sound(GAINED_POINT_SOUND, self.audo_volume);
            self.obstacle = Obstacle::new(self.player.x + SCREEN_WIDTH, self.score);
        }

        if self.player.y as i32 > SCREEN_HEIGHT || self.obstacle.hit_obstacle(&self.player) {
            self.audio_manager
                .play_sound(GAME_END_SOUND, self.audo_volume);
            self.mode = GameMode::End;
        }
    }

    pub fn restart(&mut self) {
        self.player = Player::new(5, 25);
        self.frame_time = 0.0;
        self.score = 0;
        self.obstacle = Obstacle::new(SCREEN_WIDTH, self.score);
        self.mode = GameMode::Playing;
    }

    pub fn main_menu(&mut self, ctx: &mut BTerm) {
        ctx.cls();
        ctx.print_centered(5, "Welcome to Flappy Dragon");
        ctx.print_centered(8, "(P) Play Game");
        ctx.print_centered(9, "(Q) Quit Game");

        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::P => self.restart(),
                VirtualKeyCode::Q => ctx.quitting = true, // instructs bracket-lib that you are ready to terminate the program
                _ => {}
            }
        }
    }

    pub fn dead(&mut self, ctx: &mut BTerm) {
        ctx.set_active_console(1);
        ctx.cls();

        ctx.set_active_console(0);
        ctx.cls();
        ctx.print_centered(5, "You are dead!");
        ctx.print_centered(6, &format!("You earned {} points", self.score));
        ctx.print_centered(8, "(P) Play Again");
        ctx.print_centered(9, "(Q) Quit Game");

        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::P => self.restart(),
                VirtualKeyCode::Q => ctx.quitting = true, // instructs bracket-lib that you are ready to terminate the program
                _ => {}
            }
        }
    }
}

impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        match self.mode {
            GameMode::Menu => self.main_menu(ctx),
            GameMode::End => self.dead(ctx),
            GameMode::Playing => self.play(ctx),
        }
    }
}
