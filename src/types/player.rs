use bracket_lib::prelude::*;

use super::prelude::DRAGON_FRAMES;

pub struct Player {
    pub x: i32,
    pub y: f32,
    velocity: f32,
    frame: usize, // determines "which" flap animation we are in
}

impl Player {
    pub fn new(x: i32, y: i32) -> Self {
        Self {
            x,
            y: y as f32,
            velocity: 0.0,
            frame: 0,
        }
    }

    pub fn render(&mut self, ctx: &mut BTerm) {
        // ctx.set(0, self.y, YELLOW, BLACK, to_cp437('@'))
        ctx.set_fancy(
            PointF::new(0.0, self.y),
            1,
            Degrees::new(0.0),
            PointF::new(2.0, 2.0),
            WHITE,
            NAVY,
            DRAGON_FRAMES[self.frame],
        );
    }

    pub fn gravity_and_move(&mut self) {
        if self.velocity < 2.0 {
            self.velocity += 0.2;
        }
        self.y += self.velocity;
        self.x += 1;
        if self.y < 0.0 {
            self.y = 0.0;
        }
        self.frame += 1;
        self.frame = self.frame % 6; // 5 different animation states
    }

    pub fn flap(&mut self) {
        self.velocity = -2.0;
    }
}
