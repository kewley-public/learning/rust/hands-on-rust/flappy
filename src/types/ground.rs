use bracket_lib::prelude::*;

use crate::SCREEN_WIDTH;

use super::prelude::GROUND_FRAME;

pub struct Ground {
    y_pos: i32,
}

impl Ground {
    pub fn new(y_pos: i32) -> Self {
        Self { y_pos }
    }

    pub fn render(&mut self, ctx: &mut BTerm) {
        for x in 0..SCREEN_WIDTH {
            ctx.set_fancy(
                PointF::new(x as f32, self.y_pos as f32),
                1,
                Degrees::new(0.0),
                PointF::new(1.0, 2.7),
                WHITE,
                NAVY,
                GROUND_FRAME,
            );
        }
    }
}
