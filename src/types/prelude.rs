pub use super::obstacle::Obstacle;
pub use super::player::Player;
pub use super::ground::Ground;

pub const SCREEN_WIDTH: i32 = 80;
pub const SCREEN_HEIGHT: i32 = 50;
pub const FRAME_DURATION: f32 = 75.0;
pub const DRAGON_FRAMES: [u16; 6] = [64, 1, 2, 3, 2, 1];
pub const SHADED_WALL_FRAME: u16 = 179;
pub const GROUND_FRAME: u16 = 35;
pub const FLAP_SOUND: &str = "flap";
pub const GAINED_POINT_SOUND: &str = "point";
pub const GAME_END_SOUND: &str = "game_over";
