use bracket_lib::prelude::*;

use super::prelude::{Player, SCREEN_HEIGHT, SHADED_WALL_FRAME};

pub struct Obstacle {
    // Position in world-space
    pub x: i32,
    // The center of the gap in which the dragon may pass
    gapy_y: i32,
    // Length of the gap in the obstacle
    size: i32,
}

impl Obstacle {
    pub fn new(x: i32, score: i32) -> Self {
        let mut random = RandomNumberGenerator::new();
        Obstacle {
            x,
            gapy_y: random.range(10, 40),
            size: i32::max(2, 20 - score),
        }
    }

    pub fn render(&mut self, ctx: &mut BTerm, player_x: i32) {
        let screen_x = self.x - player_x;
        let half_size = self.size / 2;

        // Draw the top half of the obstacle
        for y in 0..self.gapy_y - half_size {
            // ctx.set(screen_x, y, RED, BLACK, to_cp437('/'));
            ctx.set_fancy(
                PointF::new(screen_x as f32, y as f32),
                1,
                Degrees::new(0.0),
                PointF::new(2.0, 2.0),
                WHITE,
                NAVY,
                SHADED_WALL_FRAME,
            );
        }

        // Draw bottom half of the obstacle
        for y in (self.gapy_y + half_size)..SCREEN_HEIGHT {
            // ctx.set(screen_x, y, RED, BLACK, to_cp437('/'));
            ctx.set_fancy(
                PointF::new(screen_x as f32, y as f32),
                1,
                Degrees::new(0.0),
                PointF::new(2.0, 2.0),
                WHITE,
                NAVY,
                SHADED_WALL_FRAME,
            );
        }
    }

    pub fn hit_obstacle(&self, player: &Player) -> bool {
        let half_size = self.size / 2;
        let does_x_match = player.x == self.x;
        let player_above_gap = (player.y as i32) < self.gapy_y - half_size;
        let player_below_gap = (player.y as i32) > self.gapy_y + half_size;
        does_x_match && (player_above_gap || player_below_gap)
    }
}
